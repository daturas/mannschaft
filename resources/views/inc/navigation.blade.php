<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Titel und Schalter werden für eine bessere mobile Ansicht zusammengefasst -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Navigation ein-/ausblenden</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand">Navigation</a>
    </div>

    <!-- Alle Navigationslinks, Formulare und anderer Inhalt werden hier zusammengefasst und können dann ein- und ausgeblendet werden -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
      
      <li class="{{ url()->current() == route('mannschaften') ? 'nav-item active' : 'nav-item'}}">
      <a class="nav-link" href="{{ route('mannschaften') }}">Mannschaft hinzufügen</a>
      </li>
      
      <li class="{{ url()->current() == route('spieltermine') ? 'nav-item active' : 'nav-item'}}">
      <a class="nav-link" href="{{ route('spieltermine') }}">Spieltermin hinzufügen</a>
      </li>

      </ul>
      
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>