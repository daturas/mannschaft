<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="js/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>


    @include('inc.navigation')
<div id="wrapper" class="content-fluid">
    {{-- 
        @yield('<name>') Definiton des Inhaltbereichs. 
        Der Inhalt wird mittels @section('<name>') befüllt 
    --}}
    @yield('content')
</div>
    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/bootstrap/js/bootstrap.min.js"></script>
    @yield('js')
</body>
</html>