@extends('inc.layout')

@section('title')
{{ $title ?? 'Mannschaft hinzufügen' }}
@endsection

@section('content')
<h1 style="text-align: center">Mannschaft hinzufügen</h1>
<br>
@if(isset($success) && $success==1)
<div class="alert alert-success col-lg-4 col-lg-offset-4">Mannschaft hinzugefügt!</div>
@endif
<form action="{{ route('createMannschaft') }}" method="post">
    {{ csrf_field() }}
    <div class="form-group col-lg-4 col-lg-offset-4">
        <label for="name">Name der Mannschaft</label>
        <input type="text" name="name" class="form-control" id="name" placeholder="Name" value="{{ old('name') }}">
        @if($errors->has('name') )
        <div style="color: red;">{{$errors->first('name')}}</div>
        @endif
    </div>

    <div class="col-md-12 text-center">
        <button type="submit">Speichern</button>
    </div>
</form>
@endsection