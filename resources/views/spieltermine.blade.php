@extends('inc.layout')

@section('title')
{{ $title ?? 'Spieltermine hinzufügen' }}
@endsection

@section('content')
<h1 style="text-align: center">Spieltermin hinzufügen</h1>
<br>
@if(isset($success) && $success==1)
<div class="alert alert-success">Spieltermin hinzugefügt!</div>
@endif
<form action="{{ route('createSpieltermin') }}" method="post">
    {{ csrf_field() }}

    @if( count($select) > 0 )
    <div class="form-group col-lg-4 col-lg-offset-4">
        <label for="name">Name der Mannschaft</label>
        <select name="name" id="name" class="form-control">
            @foreach( $select as $mannschaft)
            <option value="{{ $mannschaft->id }}" @if( $mannschaft->id == old('name') ) {{ 'selected' }} @endif
                >{{ $mannschaft->name }}
            </option>
            @endforeach
        </select>
    </div>
    @endif

    <div class="form-group col-lg-4 col-lg-offset-4">
        <label for="name">Spielort</label>
        <input type="text" name="spielort" class="form-control" id="spielort" placeholder="Spielort"
            value="{{ old('spielort') }}">
        @if($errors->has('spielort') )
        <div style="color: red;">{{$errors->first('spielort')}}</div>
        @endif
    </div>

    <div class="form-group col-lg-4 col-lg-offset-4">
        <label for="spieldatum">Spieldatum</label>
        <input type="date" name="spieldatum" class="form-control" id="spieldatum">
        @if($errors->has('spieldatum') )
        <div style=" color: red;">{{$errors->first('spieldatum')}}</div>
        @endif
    </div>

    <div class="form-group col-lg-4 col-lg-offset-4">
        <label for="spielzeit">Spielzeit</label>
        <input type="time" name="spielzeit" class="form-control" id="spielzeit">
        @if($errors->has('spielzeit') )
        <div style=" color: red;">{{$errors->first('spielzeit')}}</div>
        @endif
    </div>

    <div class="col-md-12 text-center">
        <button type="submit">Speichern</button>
    </div>

    @endsection