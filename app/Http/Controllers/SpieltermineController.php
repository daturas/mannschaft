<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SpieltermineModel;
use App\MannschaftenModel;


class SpieltermineController extends Controller
{
    public function form()
    {
        $select = MannschaftenModel::orderBy('name', 'asc')->get();
 
        return view('spieltermine', compact('select'));
    }

    public function create(Request $request)
    {
        dd($request);
        $this->validate($request, [
            'spieltermin' => 'required|min:3',
            'spieldatum' => 'required',
            'spielzeit' => 'required|min:5|max:5',

        ]);

        $objSpieltermin = new SpieltermineModel($request->all());

        $objSpieltermin->save();

        return view('/spieltermine', ['success' => '1']);
    }

}