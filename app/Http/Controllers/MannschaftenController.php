<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MannschaftenModel;


class MannschaftenController extends Controller
{
    
    public function form()
    {
        return view('mannschaften');
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:3',
        ]);

        $objMannschaften = new MannschaftenModel($request->all());

        $objMannschaften->save();

        return view('/mannschaften', ['success' => '1']);
    }

    
}