<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpieltermineModel extends Model
{
    protected $table = 'spieltermins';
    protected $fillable = ['spielort', 'spieldatum', 'created_at'];
    protected $primaryKey = 'id';
}