<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MannschaftenModel extends Model
{
    protected $table = 'mannschafts';
    protected $fillable = ['name'];
    protected $primaryKey = 'id';
}