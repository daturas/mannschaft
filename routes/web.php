<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MannschaftenController@form')->name('mannschaften');
Route::post('mannschaften', 'MannschaftenController@create')->name('createMannschaft');

Route::get('/spieltermine', 'SpieltermineController@form')->name('spieltermine');
Route::post('spieltermine', 'SpieltermineController@create')->name('createSpieltermin');