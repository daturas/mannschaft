<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpielterminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('spieltermins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('spielort');
            //$table->string('gegn_mannschaft')->unique();
            $table->date('spieldatum');
            $table->string('spielzeit');
            $table->integer('mannschafts_id')->unsigned();
            $table->foreign('mannschafts_id')->references('id')->on('mannschafts')->onDelete('restrict')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spieltermins');
    }
}